import QtQuick 2.4

Item {
    width: 400
    height: 400

    Image {
        id: image
        anchors.fill: parent
        sourceSize.width: parent.width
        sourceSize.height: parent.height
        source: "qrc:/sprites/rocket_idle.svg"
        fillMode: Image.PreserveAspectFit
    }
}

import QtQuick 2.12
import QtQuick.Window 2.12

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    MapBackground {
        id: mapBackground
        anchors.fill: parent
    }

    Player {
        id: player
        x: 304
        y: 224
        height: width
        width: 64
    }
}
